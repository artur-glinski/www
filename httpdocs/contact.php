
<?php
	require_once 'php/validation.php';
?>
<!DOCTYPE html>
<html lang="pl">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link href="https://fonts.googleapis.com/css?family=Indie+Flower" rel="stylesheet">
	<!-- pisany, indie -->
	<link rel="stylesheet" href="./css/bootstrap.min.css">
	<link rel="stylesheet" href="./css/style.css">

	<script src="./js/jquery-3.2.0.min.js"></script>
	<script src="./js/validation.js"></script>

	<title>My personal website!</title>

</head>
<body id="page5">
<div class="container-fluid">

	<div class="row">
		<!-- <div class=""> -->
			<nav>
				<ul>
					<li><a href="./index.html">Home</a></li>
					<li><a href="./about.html">About&nbsp;Me</a></li>
					<li><a href="./skills.html">Skills</a></li>
					<li><a href="./projects.html">Projects</a></li>
					<li><a href="./contact.php">Contact</a></li>
				</ul>
			</nav>
		<!-- </div> -->
	</div>

	<div class="row">

		<div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4" align="">

			<form class="form-horizontal" id="form" role="form" method="post" action="contact.php" novalidate>
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label">Name:</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="name" name="name" placeholder="John"
						value="<?php echo array_key_exists('name', $_POST) ? htmlspecialchars($_POST['name']) : '' ?>">
						<p>
							<?php
								if (isset($nameError) === true) {
									echo "$nameError";
								}
							?>								
						</p>
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-sm-2 control-label">Email:</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="email" name="email" placeholder="John@Kowalski.com" 
						value="<?php echo array_key_exists('email', $_POST) ? htmlspecialchars($_POST['email']) : '' ?>">
						<p>
							<?php
								if (isset($emailError) === true) {
									echo $emailError;
								}
							?>								
						</p>
					</div>
				</div>
				<div class="form-group">
					<label for="message" class="col-sm-2 control-label">Message:</label>
					<div class="col-sm-10">
		<textarea class="form-control" id="message" name="message" placeholder="Leave a comment" rows="4"><?php echo array_key_exists('message', $_POST) ? htmlspecialchars($_POST['message']) : '' ?></textarea>
						<p>
							<?php
								if (isset($messageError) === true) {
									echo $messageError;
								}
							?>								
						</p>
					</div>
				</div>
				<div class="form-group">
					<label for="human" class="col-sm-2 control-label">3&nbsp;*&nbsp;6&nbsp;=&nbsp;?</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="human" name="human" placeholder="Your answer" 
						value="<?php echo array_key_exists('human', $_POST) ? htmlspecialchars($_POST['human']) : '' ?>">
						<p>
							<?php
								if (isset($humanError) === true) {
									echo $humanError;
								}
							?>								
						</p>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-10 col-sm-offset-2">
						<button>Send!</button>
					</div>
				</div>
			</form>

		</div>

	</div>


</body>
</html>

