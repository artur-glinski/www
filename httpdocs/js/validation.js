$(document).ready(function() {

	$("#form").submit(function(){
		// cancels the form submission
		// event.preventDefault();
		// submitForm();
	});

	$('#name').on('blur', function() {
		var nameRegExp = new RegExp('^[a-zA-Z]{3,}$', 'g');

		if (!nameRegExp.test(this.value)) {
			$(this).addClass('error');
			if ($(this).next('label').length === 0) {
				$('<label for="name">Invalid name</label>').insertAfter($(this));
			}
		} else {
			$(this).removeClass('error');
			$(this).next('label').remove();
		}
	});

	$('#email').on('blur', function() {
		var emailRegExp = new RegExp('^[0-9a-z_.-]+@[0-9a-z.-]+\.[a-z]{2,3}$', 'i');

		if (!emailRegExp.test(this.value)) {
			$(this).addClass('error');
			if ($(this).next('label').length === 0) {
				$('<label>Invalid email</label>').insertAfter($(this));
			}
		} else {
			$(this).removeClass('error');
			$(this).next('label').remove();
		}
	});

	$('#message').on('blur', function() {
		var messageRegExp = new RegExp('^[a-zA-Z \s]{6,}$', 'g');

		if (!messageRegExp.test(this.value)) {
			$(this).addClass('error');
			if ($(this).next('label').length === 0) {
				$('<label>Message is to short</label>').insertAfter($(this));
			}
		} else {
			$(this).removeClass('error');
			$(this).next('label').remove();
		}
	});

	$('#human').on('blur', function() {
		if (this.value !== '18') {
			$(this).addClass('error');
			if ($(this).next('label').length === 0) {
				$('<label>Invalid answer</label>').insertAfter($(this));
			}
		} else {
			$(this).removeClass('error');
			$(this).next('label').remove();
		}
	});

});